package net.putfirstthingsfirst.config;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import net.putfirstthingsfirst.model.SenderMessage;

@Configuration
public class SenderConfig {

	@Bean
	public TopicExchange exchange() {
		return new TopicExchange("Exchanging Message");
	}
	
	@Bean
	public SenderMessage sendmsg() {
		return new SenderMessage();
	}
}